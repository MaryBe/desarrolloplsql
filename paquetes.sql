CREATE OR REPLACE PACKAGE global_consts IS
mile_to_kilo CONSTANT NUMBER := 1.6093;
kilo_to_mile CONSTANT NUMBER := 0.6214;
yard_to_meter CONSTANT NUMBER := 0.9144;
meter_to_yard CONSTANT NUMBER := 1.0936;
END global_consts;


//hace el paquete publico
GRANT EXECUTE ON global_consts TO PUBLIC;


//se_usa_el_paquete
DECLARE
    distance_in_miles  NUMBER(5) := 5000;
    distance_in_kilo   NUMBER(6, 2);
BEGIN
    distance_in_kilo := distance_in_miles * global_consts.mile_to_kilo;
    dbms_output.put_line(distance_in_kilo);
END;

//crea_excepcion
CREATE OR REPLACE PACKAGE our_exceptions IS
e_cons_violation EXCEPTION;
PRAGMA EXCEPTION_INIT (e_cons_violation, -2292);
e_value_too_large EXCEPTION;
PRAGMA EXCEPTION_INIT (e_value_too_large, -1438);
END our_exceptions;

//hace_excepcion_publica
GRANT EXECUTE ON our_exceptions TO PUBLIC;



//crea_tabla
CREATE TABLE excep_test(number_col NUMBER(3));


//prueba_la_excepcion
BEGIN
    INSERT INTO excep_test ( number_col ) VALUES (100);
EXCEPTION
    WHEN our_exceptions.e_value_too_large THEN
        dbms_output.put_line('Value too big for column data type');
END;








